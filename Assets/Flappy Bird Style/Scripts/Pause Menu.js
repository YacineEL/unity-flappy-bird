﻿#pragma strict

var menuObject :  GameObject;
private var isActive : boolean = false;


function Update () {
	
	if(isActive == true){
		menuObject.SetActive(true);
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.Confined;
		Time.timeScale = 0;

	}else{
		menuObject.SetActive(false);
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		Time.timeScale = 1;
	}

	if(Input.GetKeyDown(KeyCode.Escape)){
		RESUME_BUTTON();
	}
}

function RESUME_BUTTON(){
	isActive = !isActive;  
}